import pytest
from selenium.webdriver import Chrome
from webdriver_manager.chrome import ChromeDriverManager

BASE_URL = "https://suresoftlimited.com"

@pytest.fixture()
def browser():
    driver = Chrome(ChromeDriverManager().install())

    driver.implicitly_wait(5)

    driver.maximize_window()

    # after tests
    yield driver
    driver.quit()