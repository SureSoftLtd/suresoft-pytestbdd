Feature

 As Frank (an interested potential client
 I want to see the services SureSoft Ltd offer
 So that I can decide if they can provide the service I need

 Scenario: Searching for the services SureSoft Ltd offer
    Given that Frank decides to look at SureSoft Ltd as a potential service provider
    When he looks at their services page
    Then he can see the services SureSoft Ltd provide plus the costs
