from pytest_bdd import (
    given,
    scenario,
    then,
    when,
)

from tests.conftest import BASE_URL
from pages.base_page import BasePage
from pages.services_page import ServicesPage
from pages.footer_page import FooterPage


@scenario('../features/feature.feature', 'Searching for the services SureSoft Ltd offer')
def test_searching_for_the_services_suresoft_ltd_offer():
    """Searching for the services SureSoft Ltd offer."""


@given('that Frank decides to look at SureSoft Ltd as a potential service provider')
def that_frank_decides_to_look_at_suresoft_ltd_as_a_potential_service_provider(browser):
    """that Frank decides to look at SureSoft Ltd as a potential service provider."""
    browser.get(BASE_URL)


@when('he looks at their services page')
def he_looks_at_their_services_page(browser):
    """he looks at their services page."""
    base_page = BasePage(browser)
    base_page.look_at_services()


@then('he can see the services SureSoft Ltd provide plus the costs')
def he_can_see_the_services_suresoft_ltd_provide_plus_the_costs(browser):
    """he can see the services SureSoft Ltd provide plus the costs."""
    services_page = ServicesPage(browser)
    assert services_page.get_title().is_displayed()
    assert services_page.get_price_title().is_displayed()
    assert services_page.get_manual_qa_price().is_displayed()
    assert services_page.get_automation_price().is_displayed()
    assert services_page.get_full_stack_price().is_displayed()
    assert services_page.get_training_price().is_displayed()
    assert services_page.get_scrum_mgnt_price().is_displayed()
    footer_page = FooterPage(browser)
    assert footer_page.look_at_contact_widget().is_displayed()
