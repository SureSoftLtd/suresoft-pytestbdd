from selenium.webdriver.common.by import By

class FooterPage:
    __FOOTER_CONTACT_WIDGET = (By.ID, 'widget_contact_info-3')

    def __init__(self, browser):
        self.browser = browser

    def look_at_contact_widget(self):
        return self.__get_contact_widget()

    def __get_contact_widget(self):
        return self.browser.find_element(*self.__FOOTER_CONTACT_WIDGET)
