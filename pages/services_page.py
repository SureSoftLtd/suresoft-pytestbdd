from selenium.webdriver.common.by import By

from pages.base_page import BasePage

class ServicesPage(BasePage):
    SERVICES_TITLE = (By.XPATH, '//*[@id="primary-header"]/div[2]/h1')
    PRICE_STRUCTURE_TITLE = (By.XPATH, '//*[@id="post-21"]/div/h2[2]')
    MANUAL_QA_PRICE = (By.XPATH, '//*[@id="post-21"]/div/p[6]')
    AUTOMATION_PRICE = (By.XPATH, '//*[@id="post-21"]/div/p[7]')
    FULL_STACK_QA_PRICE = (By.XPATH, '//*[@id="post-21"]/div/p[8]')
    TRAINING_PRICE = (By.XPATH, '//*[@id="post-21"]/div/p[9]')
    SCRUM_MGNT_PRICE = (By.XPATH, '//*[@id="post-21"]/div/p[10]')

    def get_title(self):
        return self.browser.find_element(*self.SERVICES_TITLE)

    def get_price_title(self):
        return self.browser.find_element(*self.PRICE_STRUCTURE_TITLE)

    def get_manual_qa_price(self):
        return self.browser.find_element(*self.MANUAL_QA_PRICE)

    def get_automation_price(self):
        return self.browser.find_element(*self.AUTOMATION_PRICE)

    def get_full_stack_price(self):
        return self.browser.find_element(*self.FULL_STACK_QA_PRICE)

    def get_training_price(self):
        return self.browser.find_element(*self.TRAINING_PRICE)

    def get_scrum_mgnt_price(self):
        return self.browser.find_element(*self.SCRUM_MGNT_PRICE)
