from selenium.webdriver.common.by import By

class BasePage:
    __SERVICES_LINK = (By.ID, 'menu-item-24')

    def __init__(self, browser):
        self.browser = browser

    def look_at_services(self):
        self.__get_services_page_link().click()

    def __get_services_page_link(self):
        return self.browser.find_element(*self.__SERVICES_LINK)