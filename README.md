# SureSoft Limited Test Suite - Python BDD / Selenium

## This test-suite utilises the following

 - Selenium Webdriver API
 - Pytest
 - Python BDD

## To do the following

 - Test the user interface of SureSoft Limited's own website

### Prerequisites

 - Python must be installed (add windows and Mac instructions)
 - Pipenv must be installed

### To run

 - Enter into the pipenv shell ($ pipenv shell)
 - Type the following command: $ pytest
 